import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Norge Chat",
  description: "Wiki side",
  srcDir: './src',
  lastUpdated: true,
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Hjem', link: '/' },
      { text: 'Wiki', link: '/wiki/' },
      { text: 'Kontakt', link: '/kontakt' },
      { text: 'Bli medlem', link: 'https://element.norge.chat/#/register'}
    ],
    editLink: {
      pattern: 'https://gitlab.com/norge.chat1/norge.chat-webpage/-/tree/main/src/:path',
      text: 'Gjør endringer på denne siden'
    },

    sidebar: [
      {
        text: 'Wiki',
        items: [
          { text: 'Personvern', link: '/wiki/personvern' },
          { text: 'Regler', link: '/wiki/regler' },
          { text: 'Spesifikasjoner', link: '/wiki/spesifikasjoner' },
          { text: 'Tjenester', link: '/wiki/tjenester' },
          { text: 'Kort forklart', link: '/wiki/kort-forklart' },
          { text: 'Hvem står bak', link: '/wiki/hvem-star-bak' }
        ]
      }
    ],

    outlineTitle: 'På denne siden',
    lastUpdatedText: 'Sist oppdatert',
    darkModeSwitchLabel: 'Bytt utseende',
    sidebarMenuLabel: 'Meny',
    returnToTopLabel: 'Tilbake til toppen',
    socialLinks: [
      
      { icon: 'github', link: 'https://gitlab.com/norge.chat1/norge.chat-webpage' }
    ]
  }
})
