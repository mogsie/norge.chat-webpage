# Personvernerklæring for Norge.Chat
Vi verdsetter personvernet ditt høyt og vil beskytte dine personlige opplysninger. I denne personvernerklæringen forklarer vi hvordan vi samler inn, bruker og deler personlige opplysninger om deg når du bruker vår chattetjeneste.

## Innsamling og bruk av personlige opplysninger {#innsamling-og-bruk-av-personlige-opplysninger}
Vi samler inn personlige opplysninger om deg når du registrerer deg for å bruke vår chattetjeneste, og når du bruker tjenesten. Disse opplysningene kan inkluderer navnet ditt, e-postadressen din og eventuelle andre opplysninger du velger å dele med oss.

Vi bruker disse opplysningene til å kunne tilby deg denne tjenesten, samt til å beskytte våre brukere.

## Deling av personlige opplysninger
Vi vil ikke selge, leie ut eller på annen måte avsløre personlige opplysninger til tredjeparter uten samtykke fra deg, med unntak av i følgende tilfeller:

* Vi bruker en ekstern SMTP-tjener for å sende e-post. Tjenesten vi bruker nå heter sendgrid. Du kan lese mer om deres personvernspolicy <a href="https://sendgrid.com/policies/security/">her</a>.
* Vi lagrer dine data på Hetzner sine tjenere i Finland. Du kan lese mer om deres personvernspolicy <a href="https://docs.hetzner.com/general/general-terms-and-conditions/data-privacy-faq/">her</a>
* Vi kan også dele opplysningene hvis vi er pålagt å gjøre dette ved lov, for eksempel ved en stevning eller etter en rettslig pålegg.

## Beskyttelse av personlige opplysninger
Vi tar sikkerhet på alvor og har implementert tekniske og organisatoriske tiltak for å beskytte dine personlige opplysninger mot uautorisert tilgang, endring, videreformidling eller sletting. Selv om vi gjør vårt beste for å beskytte personvernet ditt, kan vi ikke garantere at dine personlige opplysninger vil være helt sikre mot uautorisert tilgang eller bruk.

::: info
Det kan komme endringer i denne personvernerklæringen. Om du synes noe er uklart, ta gjerne <a href="mailto:kontakt@norge.chat">kontakt</a> med oss.
