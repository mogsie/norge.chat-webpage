# Regler

Vi som deg, elsker frihet. Og vi ønsker å sikre våre medlemmers ytringsfrihet. Til tross for dette, har vi noen regler vi krever at våre medlemmer følger.


* Ikke forsøk å forstyrre noen av norge.chat sine tjenester. Det være seg spamming a e-post, spamming av brukere, eller forsøke å distribuere skadelig programvare.
* Følge Norsk lov.
* Være 13 år eller eldre. (På grunn av COPPA og GDPR.
* Ingen diskriminering.

::: info
Om du synes reglene er uklare, ta gjerne kontakt.