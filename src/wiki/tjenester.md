# Tjenester

I tillegg til Matrix server har norge.chat noen ekstra funksjoner for å gjøre plattformen mer komplett.

## Klienter

* [element](https://element.norge.chat)
* [hydrogen](https://hydrogen.norge.chat)
* [cinny](https://cinny.norge.chat)

## Ekstra tjenester

* [etherpad](https://etherpad.norge.chat)
* [jitsi](https://jitsi.norge.chat)

## Funksjoner

* ntfy - Push varsel uten bruk av Google og Apple. Les mer [her](https://github.com/spantaleev/matrix-docker-ansible-deploy/blob/master/docs/configuring-playbook-ntfy.md). https://ntfy.norge.chat er vår ntfy domenenavn.
* Sliding-sync proxy - Støtte for Element x apper. Les mer [her](https://github.com/matrix-org/sliding-sync).
* Hookshot - Webhooks. Les om hvordan man bruker den [her](https://matrix-org.github.io/matrix-hookshot/latest/usage.html). Merk: Vi har ikke satt opp støtte for GitHub, GitLab, Jira, Figma. Kun generiske webhooks og feeds. Skriv til oss i [#norge-chat:norge.chat](https://matrix.to/#/#norge-chat:norge.chat), om du trenger de andre tjenestene. Så setter vi de opp. Navnet hookshot bot er @hookshot:norge.chat
