# Om tjenesten norge.chat

Norge.chat er basert på chattetjenesten [Matrix](https://matrix.org).

Matrix er en åpen standard og en desentralisert chattløsning som muliggjør sikker kommunikasjon på tvers av forskjellige plattformer og tjenester. Den er utviklet for å være et åpent og interoperabelt økosystem der brukere kan kommunisere sømløst uten å være begrenset til en spesifikk app eller plattform.

Matrix støtter i skrivende stund skrift, video, audio og opplastning av medier som videoer, bilder og dokumenter.

I stedet for å være avhengig av en sentral server eller leverandør, bruker Matrix en desentralisert arkitektur. Dette betyr at det ikke er ett enkelt punkt der all kommunikasjon går gjennom. I stedet er det flere uavhengige servere som kalles "homeservere" som er spredt over hele nettverket. Disse homeserverne kommuniserer med hverandre for å sende meldinger og opprettholde samtaler.

Matrix bruker et system med "rom" og "id'er" for å organisere samtaler. Rom er stedene der brukere kan chatte og dele informasjon. En bruker kan være medlem av flere rom samtidig. Hver bruker og rom har en unik identifikasjonskode kalt en "id". Dette gjør det enkelt å finne og invitere personer til samtaler.


Sikkerhet er også en viktig del av Matrix. Kommunikasjonen er kryptert og kan være end-to-end-kryptert, noe som betyr at bare avsender og mottaker kan lese innholdet. Dette gir en høy grad av personvern og beskyttelse av meldingene dine.

Matrix har blitt populært blant både individuelle brukere og bedrifter som ønsker en åpen og desentralisert løsning for samtaler og samarbeid. Det fortsetter å utvikle seg, og det finnes flere klientapper og tjenester som støtter Matrix-protokollen.