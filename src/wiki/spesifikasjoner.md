# Spesifikasjoner

## Maskinvare 
Serveren kjører på en 4core 8GB ram VPS fra Hetzner, lokalisert i Finland. Data er lagret på et eget storage volume, for å gjøre det lett å utvide.

## Software
Maskinen kjører Debian 11, og blir oppdatert hver mandag. Matrix stacken er konfigurert via det fantastiske prosjektet [ansible-docker-deploy](https://github.com/spantaleev/matrix-docker-ansible-deploy). Om du vil replikere vårt oppsett, kan du gjerne gjøre det. [Her er repoet til vår config](https://gitlab.com/norge.chat1/norge.chat-matrix-ansible-deploy-config).

## Backup
Backup blir tatt klokken 3 hver natt, og replikert via borg backup til [borgbase.com](https://www.borgbase.com).

## Stats
Vi bruker grafana for overvåking. Besøk [stats.norge.chat][https://stats.norge.chat] For å se på stats.