<script setup>
import { VPTeamMembers } from 'vitepress/theme'

const members = [
  {
    avatar: '/fyksen.png',
    name: 'Fredrik Fyksen',
    title: 'Ildsjel'
  }
]
</script>

# Hvem drifter og betaler for norge.chat?

Forhåpentligvis vil det komme flere på denne siden etterhvert.


<VPTeamMembers size="small" :members="members" />


::: info
For å gjøre det forutsigbart for brukere, forplikter jeg meg til å holde denne tjenesten oppe i 3 år, frem til 01.01.2026. Den 01.12.2023 åpner jeg for spleiselag, for å gjøre det mulig for andre å bidra økonomisk.
:::

## Hvor mye koster norge.chat i dag?



| **Beskrivelse** | **Sum** |
| --------------- | ------- |
| Mailserver      | 3,29 €  |
| Main server     | 13,1 €  |
| Backups         | 3 €     |
| Data Volume     | 7 €     |
| 2 IP addresser  | 1 €     |
| Domenet         | 3,5 €   |
| **Totalt**      | **30,89 €** |

