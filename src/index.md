---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Norge Chat"
  text: ""
  tagline: Et sted å kommunisere
  actions:
    - theme: brand
      text: Bli medlem
      link: https://element.norge.chat/#/register
    - theme: alt
      text: Les dokumentasjon
      link: /wiki/

features:
  - title: Fritt
    details: norge.chat er gratis å bruke, og bruker åpen kildekode
  - title: Hvorfor finnes denne tjeneren?
    details: Vi ønsker det skal finnes en plattform som gjør det mulig å chatte med hverandre uten at et stort selskap selger dataene dine.
  - title: Ideell organisasjon?
    details: På sikt ønsker vi at denne tjenesten (og muligens andre) drives av en organisasjon, og ikke en privatperson. Om du kunne tenke deg å bidre her, gi gjerne en lyd fra deg.
---

